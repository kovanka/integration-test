import { Router }  from "express";
import { hex_to_rgb, rgb_to_hex} from './converter.js'

const routes = Router();

// Endpoit GET '{{api_url}}/'
routes.get('/', (req, res) => res.status(200).send("Welcome!"));

// Endpoint GET /rgb-to-hex?red=255&green=136&blue=0
routes.get('/rgb-to-hex', (req, res) => {
    const RED = parseFloat(req.query.red);
    const GREEN = parseFloat(req.query.green);
    const BLUE = parseFloat(req.query.blue);
    // TODO:
    // tarkista että luvut väliltä 0-255
    const HEX = rgb_to_hex(RED, GREEN, BLUE); // integraatio
    res.status(200).send(HEX);
});

// Endpoint GET /hex-to-rgb?hex=#ff8800
routes.get('/hex-to-rgb', (req, res) => {
    const HEX = req.query.hex;
    // TODO:
    // tarkista että #000000-#ffffff välillä
    const RGB = hex_to_rgb(HEX);
    res.status(200).send(RGB);
});



export default routes;